const sendMail= require('../services/mail');

const sendMessage = async (req, res) => {
    try{
        const message = {
            to: req.body.to,
            subject: req.body.subject,
            html: req.body.email_body,
        };
        
        await sendMail(message);

        return res.status(200).json({
            "success" : true,
            "message" : "Email sent successfully"
        })
    
    }
    catch(err){
        return res.status(500).json({
            "success" : false,
            "message" : err.message
        })
    }
}

module.exports = {
    sendMessage
}