module.exports = {
    app : {
        port : process.env.PORT || 8000
    },
    mail : {
        address : process.env.EMAIL_ADDRESS,
        password : process.env.EMAIL_PASSWORD
    }
}