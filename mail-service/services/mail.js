const nodemailer = require("nodemailer");
const config = require('../config');

const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: config.mail.address,
        pass: config.mail.password
    }
});

let sendMail = (message) => {
    return transporter.sendMail(message);
}

module.exports = sendMail;