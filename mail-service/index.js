const express = require('express');
const cors = require('cors');
const controller = require('./controllers/mail');
const morgan = require('morgan');
const app = express();


app.use(cors());
app.use(morgan('tiny'));
app.use(express.urlencoded({extended:false}));
app.use(express.json());

app.post("/", controller.sendMessage);

app.get("/ping", (req, res) => {
    return res.send({
        status : "Healthy"
    });
});

app.use((req, res, next)=>{
	const err = new Error("not found")
	err.status = 404
	next(err);
})

app.use((error, req, res, next)=>{
	return res.status(error.status || 500).json({
		error: {
			message: error.message
		}
	});
})

module.exports = app;