# Install the dependencies using : ```npm install```
# Run using : ```npm start```
# The api runs at ```http://localhost:8000/``` by default.

# Add a .env file which contains :
``` 
EMAIL_ADDRESS  = Email address(gmail) from which mails can be sent.
EMAIL_PASSWORD = Password of the email address.

Note that you have to enable your account for less secure apps at https://myaccount.google.com/lesssecureapps to send email

PORT           = Port at which the application should run. By default the application runs at port 8000
```
