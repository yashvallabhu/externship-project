# To install dependencies and run mail service check the [readme](https://gitlab.com/yashvallabhu/externship-project/-/tree/master/mail-service) file.
# To install dependencies and run photo-service check the [readme](https://gitlab.com/yashvallabhu/externship-project/-/tree/master/photo-service) file.
# To install dependencies and run src check the [readme](https://gitlab.com/yashvallabhu/externship-project/-/tree/master/src) file.

# Add .env file for all the services as mentioned in the repective readme files.

# While running the src, make sure that email service, photo-service is running.