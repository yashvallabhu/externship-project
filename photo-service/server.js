require('dotenv').config();
const config = require('./config');
const app = require('./index.js');

const PORT = config.app.port

app.listen(PORT, function(){
    console.log(`Server started at port ${PORT}`);
});

process.on('unhandledRejection', error => {
    console.log('unhandledRejection', error);
});

process.on('uncaughtException', function (err) {
    console.error(err);
});