const express = require('express');
const cors = require('cors');
const morgan = require('morgan');
const app = express();
const fileRouter = require('./routes/files'); 

app.use('/images', express.static('Images'));

app.use(cors());
app.use(morgan('tiny'));
app.use(express.urlencoded({extended:false}));
app.use(express.json());

app.use("/", fileRouter);

app.get("/ping", (req, res) => {
    return res.send({
        status : "Healthy"
    });
});

app.use((req, res, next)=>{
	const err = new Error("not found")
	err.status = 404
	next(err);
})

app.use((error, req, res, next)=>{
	return res.status(error.status || 500).json({
		error: {
			message: error.message
		}
	});
})

module.exports = app;