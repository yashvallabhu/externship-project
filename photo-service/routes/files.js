const express = require("express");
const router = express.Router();

const controller = require("../controllers/files")
const upload = require("../middlewares/file.upload");

router.post('/upload', upload.single('image'), controller.uploadPic);
router.delete('/delete/:filename', controller.deleteFile);

module.exports = router