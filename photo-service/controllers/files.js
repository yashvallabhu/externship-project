const config = require('../config');
const fs = require('fs');

const { promisify } = require('util');
const unlinkAsync = promisify(fs.unlink);
module.exports.uploadPic = async (req, res, next) => {
    try{
        if (req.file == undefined){
            const err = new Error("file is required");
            err.status = 400;
            throw err;
        }
        const url = `http://${req.hostname}:${config.app.port}/images/${req.file.filename}`;
        
        return res.status(201).send(url);
    }
    catch(e){
        next(e);
    }
}

module.exports.deleteFile = async (req, res, next) => {
    try {
        const path = './Images/' + req.params.filename;
        if (!fs.existsSync(path)){
            return res.status(200).send("No such file exists");
        }
        await unlinkAsync(path);
        return res.status(200).send("Successfully deleted file");
    } catch (e) {
        next(e);
    }
}