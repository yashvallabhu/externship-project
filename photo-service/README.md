# Install the dependencies using : ```npm install```
# Run using : ```npm start```
# The api runs at ```http://localhost:4000/``` by default.

# Add a .env file which contains :
``` 
PORT           = Port at which the application should run. By default the application runs at port 4000
```
