const mongoose = require("mongoose");
const config = require('./config');
const DB_URL = config.db.URL;

mongoose.connect(DB_URL, {
	useNewUrlParser: true,
	useUnifiedTopology: true
}).then(() => {
    console.log("Successfully connected to Database!");
}).catch(err => {
    console.error("Error connecting to database" + err);
    process.exit(1)
});

module.exports = mongoose.connection

