module.exports = {
    app : {
        PORT : process.env.PORT || 3000
    },
    db : {
        PORT : process.env.DB_PORT || 27017,
        URL : process.env.DB_URL || "mongodb://localhost:27017/flipr",
        NO_OF_SALT_ROUNDS : process.env.NO_OF_SALT_ROUNDS || 10,
        PAGINATION_PAGE_SIZE : process.env.PAGINATION_PAGE_SIZE || 10
    },
    jwt : {
        KEY : process.env.JWT_KEY || "hfubwaAJBCJBVB",
        expiry : {
            LOGIN : process.env.JWT_LOGIN_EXPIRY || '1d',
            ACTIVATION : process.env.ACTIVATION_EXPIRY || '20m',
            FORGOT_PASSWORD : process.env.FORGOT_PASSWORD_EXPIRY || '5m'
        },
        types : {
            LOGIN : 'login',
            FORGOT_PASSWORD : 'forgot-password',
            ACTIVATION : 'activation'
        }
    },
    email : {
        URL : process.env.EMAIL_SERVICE_URL || `http://localhost:8000/`
    },
    photo: {
        URL : process.env.PHOTO_SERVICE_URL || `http://localhost:4000/`,
        uploadUrl : (process.env.PHOTO_SERVICE_URL || `http://localhost:4000/`) + 'upload',
        deleteUrl : (process.env.PHOTO_SERVICE_URL || `http://localhost:4000/`) + 'delete/',
    }
}