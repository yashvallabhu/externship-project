# Install the dependencies using : ```npm install```
# Run using : ```npm start```
# The api runs at ```http://localhost:3000/``` by default.

# This uses gmail service to send emails

# Add a .env file which contains :
``` 
PORT                    = Port at which the application should run. By default the application runs at port 3000,
DB_URL                  = Mongodb url. By default it uses "mongodb://localhost:27017/flipr".
NO_OF_SALT_ROUNDS       = No of salt rounds to hash the password. By default it uses 10 rounds.
JWT_KEY                 = Jwt key which is used to sign tokens.
JWT_LOGIN_EXPIRY        = Expiry time of login token. By default it is '1d'.
ACTIVATION_EXPIRY       = Expiry time of account activation token. By default it is '20m'
FORGOT_PASSWORD_EXPIRY  = Expiry time of forgot password token. By default it is '5m'
PAGINATION_PAGE_SIZE    = Page size (limit number of results). By defaut it is 10.
EMAIL_SERVICE_URL       = URL at which the email service (which is present in the previous directory) is running. By default it uses http://localhost:8000/
PHOTO_SERVICE_URL       = URL at which the photo service (which is present in the previous directory) is running. By default it uses http://localhost:4000/
```

# Routes
### POST ```/users/signup```
```
Request body should contain a JSON object of:
{
    "username" : your_username,
    "password" : your_password,
    "email" : your_email,
}

On a successful request, an activation link with 20 min expiry is sent to user's email.
```

### POST ```/users/login```
```
Request body should contain a JSON object of:
{
    "username" : your_username,
    "password" : your_password
}

On a successful request, a authentication token is returned.
```

### GET ```/users/activate_account/:token```
```
User can activate his/her account using the link sent to their email during signup.
```

### GET ```/users/forgot_password```
```
Request body should contain a JSON object of:
{
    "email" : your_email,
}
On a successful request, a reset_password link with 5 min expiry is sent to user's email.
```

### POST ```/users/forgot_password/:token```
```
{
    "new_password" : your_new_password,
    "confirm_password" : enter the password again
}
```

### GET ```/users/```
```
Gets the details of the users. You can also filter users using query paramters such as limit, email, username
```

### GET ```/users/:id```
```
Get the details of the user with the given id.
```

### PUT ```/users/:id```
```
User requires authorization to update details. The body the this request can contain one/more of the following: 
{
    "isPublic" : boolean,
    "first_name" : String,
    "last_name" : String
}
```
