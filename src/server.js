require('dotenv').config();
const app = require('./api/index.js');
const config = require("./config/config.js");
const db = require('./config/db');

const PORT = config.app.PORT;

db.on('error', console.error.bind(console, 'MongoDB connection error:'))

app.listen(PORT, function(){
    console.log(`Server started at port ${PORT}`);
});

process.on('unhandledRejection', error => {
    console.log('unhandledRejection', error);
});

process.on('uncaughtException', function (err) {
    console.error(err);
});