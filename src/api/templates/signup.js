const config = require("../../config/config");

module.exports = (hostname, username, token) => {
    return `<strong> 
        Hello ${username}. Thank you for signing up to our website.
        Use the following link to activate your account.
        <br>
        <a href= http://${hostname}:${config.app.PORT}/users/activate_account/${token}> 
            http://${hostname}:${config.app.PORT}/users/activate_account/${token}
        </a>
        <br>
    </strong>`
}