const config = require("../../config/config");

module.exports = (hostname, username, token) => {
    return `<strong> 
        Hello ${username}. You can use the following link to set a new password.
        <br>
        <a href= http://${hostname}:${config.app.PORT}/users/forgot_password/${token}> 
            http://${hostname}:${config.app.PORT}/users/forgot_password/${token}
        </a>
        <br>
    </strong>`
}