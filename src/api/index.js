const express = require("express");
const app = express();
const morgan = require('morgan');

const userRouter = require('./v1/routes/user_router');
const friendRouter = require("./v1/routes/friends.router");
const imageRouter = require("./v1/routes/images.router");
const postRouter = require('./v1/routes/posts.router');
const commentRouter = require('./v1/routes/comments.router');

app.use(morgan('tiny'));

app.use(express.urlencoded({extended:false}));;
app.use(express.json())

app.use('/users', userRouter);
app.use('/friends', friendRouter);
app.use("/images", imageRouter);
app.use('/posts', postRouter);
app.use('/comments', commentRouter);

app.get("/ping", (req, res) => {
    return res.send({
        status : "Healthy"
    })
});

app.use((req, res, next)=>{
	const err = new Error("not found")
	err.status = 404
	next(err)
})

app.use((error, req, res, next)=>{
	return res.status(error.status || 500).json({
		error: {
			message: error.message
		}
	})
})

module.exports = app