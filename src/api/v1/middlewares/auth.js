const tokenService = require('../services/tokenService');
const config = require("../../../config/config");
const userService = require("../services/userService");
const port = config.app.PORT;

module.exports = async (req, res, next) => {
	try{
		const token = req.headers.authorization.split(" ")[1];
		const [err, details] = tokenService.verifyToken(token, config.jwt.types.LOGIN);
		
		if(err){
			throw new Error();
		}
		const user = await userService.getUserbyId(details.user_id);

		if(!user){
			throw new Error();
		}
		
		req.user = user;
		next();
	}
	catch(err){
        return res.status(401).json({
			message: "Authorization failed! Use the following link to login",
			url: `http://${req.hostname}:${port}/users/login`
		})
	}
}