const FormData = require('form-data');
const axios = require('axios');
const config = require("../../../config/config");

module.exports.uploadImage = async (file) => {
    try{
        let form = new FormData();
        form.append('image', file.buffer, file.originalname);
        
        const response = await axios.post(config.photo.uploadUrl, form, {
            headers: {
                'Content-Type': `multipart/form-data; boundary=${form._boundary}`
            }
        });

        return [null, response];
    }
    catch(e){
        return [e, null];
    }
}

module.exports.deleteImage = async (file_path) => {
    try {
        const arr = file_path.split("/");
        const filename = arr[arr.length - 1];
        await axios.delete(config.photo.deleteUrl + filename);
        return null;
    } catch (e) {
        return e;
    }
}