const Username = require("../models/username");

const findUsername = async (username) => {
    const uname = await Username.find({username: username});
    return uname;
}

const createUsername = async (username) => {
    const uname = await Username.create({
        username: username
    });
    return uname;
}

const deleteUsername = async (username) => {
    await Username.deleteOne({username : username});
}

module.exports = {
    findUsername,
    createUsername,
    deleteUsername
}