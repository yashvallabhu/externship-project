const {User} = require("../models/user");
const FriendRequest = require("../models/friend.request");
const config = require('../../../config/config');

module.exports.checkFriend = (requested_user, friend_user_id) => {
    if(requested_user.friends.includes(friend_user_id)){
        return true;
    }
    return false;
}

module.exports.createRequest = async (requested_user_id, friend_id) => {
    const request = new FriendRequest({
        to: friend_id,
        from: requested_user_id
    })
    await request.save();
}

module.exports.findRequest = async (friend_id, requested_user_id) => {
    const request = await FriendRequest.find({from: friend_id, to: requested_user_id});
    if(request.length > 0)
        return true;
    return false;
}

module.exports.getRequestsSentToUser = async (user_id) => {
    const requests = await FriendRequest.find({to: user_id});
    return requests;
}

module.exports.getRequestsSentByUser = async (user_id) => {
    const requests = await FriendRequest.find({from: user_id});
    return requests;
}

module.exports.findRequestbyId = async (id) => {
    const request = await FriendRequest.findById(id);
    return request;
}

module.exports.deleteRequest = async (id) => {
    await FriendRequest.deleteOne({id: id});
}

module.exports.acceptRequest = async (request) => {
    const user1 = await User.findById(request.to);
    const user2 = await User.findById(request.from);
    user1.friends.push(user2.id);
    await user1.save();
    user2.friends.push(user1.id);
    await user2.save();
    await FriendRequest.deleteOne({id: request.id}); 
}

module.exports.rejectRequest = async (id) => {
    await FriendRequest.deleteOne({id: id});
}

module.exports.getFriends = async (usr, page) => {
    const page_size = config.db.PAGINATION_PAGE_SIZE;
    const skip_docs = (page-1)*page_size;

    const user = await User.findById(usr.id).populate({
        path : 'friends',
        options : {
            skip : skip_docs,
            limit: page_size
        },
        select : 'id username email'
    }).select('friends -_id');
    
    return user.friends;
}

module.exports.removeFriend = async (user1_id, user2_id) => {
    const user1 = await User.findById(user1_id);
    user1.friends.pull(user2_id);
    user1.save();

    const user2 = await User.findById(user2_id);
    user1.friends.pull(user1_id);
    user2.save();
}

module.exports.suggestFriends = async (user) => {
    const user_friends = [...user.friends];
    user_friends.push(user.id);
    
    var suggestions = await User.findById(user.id).populate({
        path: 'friends',
        populate: {
            path : 'friends',
            match : {
                _id : { $nin : user_friends} 
            }
        }
    });

    suggestions = suggestions.friends.map(frd => {
        return frd.friends
    })
    
    suggestions = suggestions.flat(2);

    suggestions = suggestions.map(frd => {
        return {
            id : frd.id,
            username : frd.username
        }
    })
    return suggestions;
    ;
}