const config = require("../../../config/config");
const axios = require('axios');
  
const sendMail = async(msg) => {
    return await axios.post(config.email.URL, msg);
       
}

module.exports = sendMail;