const bcrypt = require('bcryptjs');
const {User, userDetailsValidate } = require("../models/user");
const unameService = require("./unameService");
const config = require("../../../config/config");

const findUsersbyUsernameorEmail = async (username, email) => {
    const result = await User.find({$or : [
                        {'username': username},
                        {'email' : email}
                    ]});
    return result;
}

const findExistingUsers = async (username, email) => {
    const result1 = await findUsersbyUsernameorEmail(username, email);
    const result2 = await unameService.findUsername(username);

    if(result1.length > 0 || result2.length > 0)
        return true;
    else
        return false;
} 

const validateDetails = (details) =>  {
    
    if(! (details.username && details.password && details.email) ){
        var err = new Error("Username, email and password fields are required")
        err.status = 400
        return err;
    }

    var err = userDetailsValidate({
        username : details.username,
        hash : String(details.password),
        email : details.email
    });
    
    if(err.error){
        const e = new Error(err.error.details[0].message)
        e.status = 400
        return e;
    }
    return false;
}

const createUser = async (username, email, hash, isVerified=false) => {
    await unameService.deleteUsername(username);

    const user = await User({
        username : username,
        hash : hash,
        email : email,
        isVerified : isVerified
    });
    user.save();
    return user;
}

const findUserandVerifyPassword = async (username, password) => {
    const user = await User.findOne({username : username});
    var verify = false;
    if(user){
        verify = await bcrypt.compare(String(password), user.hash);
    }
    return verify ? user : null;
    
}

const getUsers = async (query) => {
    let users = [];
    const limit = query.limit ? query.limit : 100
    const options = { sort: { 'username': 'asc' }, limit: parseInt(limit)}
    
    if(query.email)
        users = await User.find({email : query.email}, null, options);
    else if(query.username){
        const regex = new RegExp(query.username, 'i')
        users = await User.find({username : {$regex : regex}}, null, options);
    }
    else
        users = await User.find({}, null, options);
    
    users = users.map(user => {
        return {
            _id : user.id,
            username : user.username
        }
    })

    return users;
}

const getUserbyId = async (id) => {
    const user = await User.findById(id);
    return user;
}

const getUserbyEmail = async (email) => {
    const user = await User.findOne({email : email});
    return user;
}

const getDetails = async (id) => {
    const user = await getUserbyId(id);

    return user.isPublic ? {
        id : user.id,
        username : user.username,
        email: user.email,
        first_name : user.first_name || "",
        last_name : user.last_name || "",
        visibility : "public"
    } : {
        id : user.id,
        username : user.username,
        visibility : "private"
    }
}

const updateUserProfile = async (id, details) => {
    var err = false;
    if(details.username || details.email || details.isVerified || details.hash){
        err = new Error("Username or Email or isVerified cannot be updated");
        err.status = 405
        return err;
    }
    
    await User.findByIdAndUpdate({_id : id}, details);

    return err;
}

const updateUserPassword = async (id, password) => {
    const encrypt = await bcrypt.hash(String(password), config.db.NO_OF_SALT_ROUNDS);

    await User.findByIdAndUpdate({_id : id}, {hash : encrypt});
}

const updateProfilePic = async (id, url) => {
    const user = await User.findById(id);
    user.profile_pic = url;
    user.save();
}

module.exports = {
    findUsersbyUsernameorEmail,
    createUser,
    getUsers,
    getUserbyId,
    getUserbyEmail,
    validateDetails,
    findExistingUsers,
    findUserandVerifyPassword,
    getDetails,
    updateUserProfile,
    updateUserPassword,
    updateProfilePic
}