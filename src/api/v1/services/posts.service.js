const Post = require("../models/post");
const photoService = require("./photo.service");
const config = require('../../../config/config');
module.exports.getPosts = async (user_id) => {
    const posts = await Post.find({author: user_id});
    return posts.map(post => {
        return {
            id: post.id,
            image : post.image,
            description : post.description,
            likes : post.likes.length,
            created_at : post.created_at,
            updated_at : post.updated_at
        }
    });
}

module.exports.getPostById = async (id) => {
    const post = await Post.findById(id);
    return post;
}

module.exports.createPost = async (user, body) => {
    const post = new Post({
        author : user.id,
        image : body.image_url,
        description : body.description,
    })
    await post.save();
}

module.exports.updatePost = async (post_id, body) => {
    const post = await Post.findById(post_id);
    const update = {
        image : body.image_url || post.image,
        description: body.description || post.description
    }
    if(update.image != post.image){
        await photoService.deleteImage(post.image);
    }
    await Post.findOneAndUpdate({id: post_id}, update);
}

module.exports.deletePost = async (id, image_url) => {
    await Post.deleteOne({id: id});
    await photoService.deleteImage(image_url);
}

module.exports.likePost = async (user_id, post_id) => {
    const post = await Post.findById(post_id);
    if(!post.likes.includes(user_id)){
        post.likes.push(user_id);
        await post.save();
        return true;
    }
    else{
        post.likes.pull(user_id);
        await post.save();
        return false;
    }   
}

module.exports.addComment = async (post_id, comment_id) => {
    const post = await Post.findById(post_id);
    post.comments.push(comment_id);
    await post.save();
}

module.exports.getFeed = async (user, page) => {
    const page_size = config.db.PAGINATION_PAGE_SIZE;
    const skip_docs = (page-1)*page_size;

    var posts = await Post.find({author : {$in : user.friends}}).populate({
        path: 'author',
        select: 'username -_id'
    }).select('-comments').sort({'created_at': 'desc'}).skip(skip_docs).limit(page_size);

    posts = posts.map(post => {
        return {
            "id" : post.id,
            "author" : post.author.username,
            "image" : post.image,
            "description" : post.description,
            "likes" : post.likes.length,
            "created_at" : post.created_at,
            "updated_at" : post.updated_at
        }
    })

    return posts;
}