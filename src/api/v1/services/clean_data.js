const sanitize = require('mongo-sanitize');

const cleanData = (body) => {
    return sanitize(body);
}

module.exports = cleanData;