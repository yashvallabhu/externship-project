const Comment = require("../models/comment");
const Post = require('../models/post');
const postService = require('./posts.service');

module.exports.getCommentsofPost = async (id) => {
    const comments = await Post.findById(id).populate({
        path: 'comments',
        populate: {
            path: 'author',
            select: 'username -_id'
        }
    }).select('comments -_id');
    
    return comments;
}   

module.exports.createComment = async (post_id, user_id, body) => {
    const comment = new Comment({
        author : user_id,
        comment : body.comment,
    })
    await comment.save();
    
    await postService.addComment(post_id, comment.id);
}

module.exports.getCommentbyId = async (id) => {
    const comment = await Comment.findById(id);
    return comment;
}

module.exports.updateComment = async (comment_id, body) => {
    await Comment.findOneAndUpdate({id: comment_id}, {comment: body.comment});
}

module.exports.deleteComment = async (id) => {
    await Comment.deleteOne({id: id});
}
