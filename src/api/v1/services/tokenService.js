const jwt = require("jsonwebtoken");
const bcrypt = require('bcryptjs');
const config = require("../../../config/config");
const unameService = require("./unameService");

const createActivationToken = async (username, password, email) => {
    await unameService.createUsername(username);
    const encrypt = await bcrypt.hash(String(password), config.db.NO_OF_SALT_ROUNDS);

    const token = jwt.sign({
        username : username,
        hash : encrypt,
        type: config.jwt.types.ACTIVATION,
        email : email
    }, config.jwt.KEY, {
        expiresIn : config.jwt.expiry.ACTIVATION
    });
    return token;
}

const createAuthToken = (user) => {
    const token = jwt.sign({
        user_id : user.id,
        email: user.email,
        type: config.jwt.types.LOGIN
    },
    config.jwt.KEY, {
        expiresIn : config.jwt.expiry.LOGIN
    })
    return token;
}

const createForgotPasswordToken = (user) => {
    const token = jwt.sign({
        user_id : user.id,
        type : config.jwt.types.FORGOT_PASSWORD
    },
    config.jwt.KEY, {
        expiresIn : config.jwt.expiry.FORGOT_PASSWORD
    })
    return token;
}

const verifyToken = (token, tokenType) => {
    const details = jwt.verify(token, config.jwt.KEY);
    
    if(details.type != tokenType)
        return [true, null]
    else   
        return [false, details]
}

module.exports = {
    createActivationToken,
    createAuthToken,
    createForgotPasswordToken,
    verifyToken
}