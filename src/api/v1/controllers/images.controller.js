const userService = require('../services/userService');
const photoService = require('../services/photo.service');
module.exports.uploadProfilePic = async (req, res, next) => {
    try {
        if (req.file == undefined){
            const err = new Error("file is required");
            err.status = 400;
            throw err;
        }

        const [err, response] = await photoService.uploadImage(req.file);

        if(err){
            throw err;
        }
        if(req.user.profile_pic){
            await photoService.deleteImage(req.user.profile_pic);
        }
        await userService.updateProfilePic(req.user.id, response.data);

        return res.status(201).send("Successfully uploaded image");

    } catch (e) {
        next(e);
    }
}

module.exports.uploadPostImage = async (req, res, next) => {
    try {
        if (req.file == undefined){
            const err = new Error("file is required");
            err.status = 400;
            throw err;
        }

        const err = await photoService.uploadImage(req.file);

        if(err){
            throw err;
        }

        return res.status(200).json({
            "success" : true,
            'message' : "Successfully delete image"
        });
    } catch (e) {
        next(e);
    }
}