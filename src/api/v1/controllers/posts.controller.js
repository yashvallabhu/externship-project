const mongoose = require('mongoose');
const userService = require("../services/userService");
const friendService = require('../services/friends.service');
const postService = require('../services/posts.service');

module.exports.getPosts = async (req, res, next) => {
    try {
        if(!mongoose.Types.ObjectId.isValid(req.params.id)){
            return res.status(400).send("Invalid id");
        }
        const user = await userService.getUserbyId(req.params.id);
        if(!user){
            var err = new Error("No user with the given id exists");
            err.status = 400;
            throw err;
        }
        if(req.user.id == user.id){
            const posts = await postService.getPosts(req.params.id);
            return res.status(200).send(posts);
        }
        const checkFriend = friendService.checkFriend(req.user, req.params.id);
        if(!(user.isPublic || checkFriend)){
            var err = new Error('User profile is private. You cannot view their posts');
            err.status = 401;
            throw err;
        }
        const posts = await postService.getPosts(req.params.id);
        return res.status(200).send(posts);
    } catch (e) {
        next(e);
    }
}

module.exports.createPost = async (req, res, next) => {
    try {
        if(!req.body.image_url || !req.body.description){
            return res.status(400).send('Both image and description fields are required !!');
        }
        await postService.createPost(req.user, req.body);
        return res.status(201).json({
            "success" : true,
            "message" : "Successfully created post"
        });
    } catch (e) {
        next(e);
    }
}

module.exports.updatePost = async (req, res, next) => {
    try {
        if(!mongoose.Types.ObjectId.isValid(req.params.id)){
            return res.status(400).send("Invalid id");
        }
        const post = await postService.getPostById(req.params.id);
        if(!post){
            var err = new Error("No post with the given id exists");
            err.status = 400;
            throw err;
        }
        if(req.user.id != post.author){
            var err = new Error("You are not authorized to update post");
            err.status = 401;
            throw err;
        }
        await postService.updatePost(req.params.id, req.body);
        return res.status(200).json({
            "success" : true,
            "message" : "Successfully updated post"
        });
    } catch (e) {
        next(e);
    }
}

module.exports.deletePost = async (req, res, next) => {
    try {
        if(!mongoose.Types.ObjectId.isValid(req.params.id)){
            return res.status(400).send("Invalid id");
        }
        const post = await postService.getPostById(req.params.id);
        if(!post){
            var err = new Error("No post with the given id exists");
            err.status = 400;
            throw err;
        }
        if(req.user.id != post.author){
            var err = new Error("You are not authorized to update post");
            err.status = 401;
            throw err;
        }
        await postService.deletePost(req.params.id, post.image);
        return res.status(200).json({
            "success" : true,
            "message" : "Successfully deleted post"
        });
    } catch (e) {
        next(e);
    }
}

module.exports.likePost = async (req, res, next) => {
    try {
        if(!mongoose.Types.ObjectId.isValid(req.params.id)){
            return res.status(400).send("Invalid id");
        }
        const post = await postService.getPostById(req.params.id);
        if(!post){
            var err = new Error("No post with the given id exists");
            err.status = 400;
            throw err;
        }
        const checkFriend = friendService.checkFriend(req.user, post.author);
        if(!(post.author == req.user.id) && !(post.author.isPublic || checkFriend)){
            var err = new Error('User profile is private. You cannot view/like their posts');
            err.status = 401;
            throw err;
        }
        const b = await postService.likePost(req.user.id, req.params.id);
        return res.status(200).json({
            "success" : true,
            "message" : b ? "Successfully liked post" : "Successfully unliked post"
        })

    } catch (e) {
        next(e);
    }
}

module.exports.getFeed = async (req, res, next) => {
    try{
        const page = req.query.page || 1;
        const posts = await postService.getFeed(req.user, page);
        return res.status(200).send(posts);
    }
    catch(e){
        next(e);
    }
}