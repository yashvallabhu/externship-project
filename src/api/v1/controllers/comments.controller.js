const mongoose = require('mongoose');
const postService = require('../services/posts.service');
const userService = require('../services/userService');
const friendService = require('../services/friends.service');
const commentService = require('../services/comments.service');
module.exports.getComments = async(req, res, next) => {
    try {
        if(!mongoose.Types.ObjectId.isValid(req.params.id)){
            return res.status(400).send("Invalid id");
        }
        const post = await postService.getPostById(req.params.id);
        if(!post){
            return res.status(400).send("No post with the given id exists");
        }
        const post_author = await userService.getUserbyId(post.author);
        const checkFriend = friendService.checkFriend(req.user, post.author);
        if(!post_author.isPublic && !checkFriend){
            var err = new Error("You are not authorized to view the comments on this post because this is private");
            err.status = 401;
            throw err;
        }
        const comments = await commentService.getCommentsofPost(req.params.id);
        return res.status(200).send(comments);
    } catch (e) {
        next(e);
    }
}

module.exports.createComment = async(req, res, next) => {
    try {
        if(!mongoose.Types.ObjectId.isValid(req.params.id)){
            return res.status(400).send("Invalid id");
        }
        const post = await postService.getPostById(req.params.id);
        if(!post){
            return res.status(400).send("No post with the given id exists");
        }
        const post_author = await userService.getUserbyId(post.author);
        const checkFriend = friendService.checkFriend(req.user, post.author);
        if(!post_author.isPublic && !checkFriend){
            var err = new Error("You are not authorized to comment on this post as the author's profile is private. Add author as a friend to comment");
            err.status = 401;
            throw err;
        }
        if(!req.body.comment){
            return res.status(400).send('Comment is required !!');
        }
        await commentService.createComment(req.params.id, req.user.id, req.body);
        return res.status(201).json({
            "success" : true,
            "message" : "Successfully created comment"
        });
    } catch (e) {
        next(e);
    }
}

module.exports.updateComment = async(req, res, next) => {
    try {
        if(!mongoose.Types.ObjectId.isValid(req.params.id)){
            return res.status(400).send("Invalid id");
        }
        const comment = await commentService.getCommentbyId(req.params.id);
        if(!comment){
            return res.status(400).send("No comment with the given id exists");
        }
        if(comment.author != req.user.id){
            var err = new Error("You are not authorized to update comment");
            err.status = 401;
            throw err;
        }
        if(!req.body.comment){
            return res.status(400).send('Comment is required !!');
        }
        await commentService.updateComment(req.params.id, req.body);
        return res.status(200).json({
            "success" : true,
            "message" : "Successfully updated comment"
        });
    } catch (e) {
        next(e);
    }
}

module.exports.deleteComment = async(req, res, next) => {
    try {
        if(!mongoose.Types.ObjectId.isValid(req.params.id)){
            return res.status(400).send("Invalid id");
        }
        const comment = await commentService.getCommentbyId(req.params.id);
        if(!comment){
            return res.status(400).send("No comment with the given id exists");
        }
        if(comment.author != req.user.id){
            var err = new Error("You are not authorized to delete comment");
            err.status = 401;
            throw err;
        }
        await commentService.deleteComment(req.params.id);
        return res.status(200).json({
            "success" : true,
            "message" : "Successfully deleted comment"
        });
    } catch (e) {
        next(e);
    }
}