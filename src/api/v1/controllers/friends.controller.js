const mongoose = require('mongoose');
const friendService = require("../services/friends.service");
const userService = require("../services/userService");

module.exports.sendRequest = async (req, res, next) => {
    try {
        if(!mongoose.Types.ObjectId.isValid(req.body.user_id)){
            return res.status(400).send("Invalid id");
        }
        const checkUser = userService.getUserbyId(req.body.user_id);
        if(!checkUser){
            return res.status(400).json({
                message : "The user with the given id doesnot exists"
            })
        }

        const ex = friendService.checkFriend(req.user, req.body.user_id);
        if(ex){
            return res.status(200).json({
                message : "This user is already your friend."
            })
        }
        
        const opp_request = await friendService.findRequest(req.body.user_id, req.user.id);
        if(opp_request){
            return res.status(200).json({
                message: "The user already sent a friend request to you. You can accept it."
            })
        }
        
        const ex_request = await friendService.findRequest(req.user.id, req.body.user_id);
        if(ex_request){
            return res.status(200).json({
                message: "You have already sent a friend request to the user."
            })
        }
        
        await friendService.createRequest(req.user.id, req.body.user_id);
        
        return res.status(200).json({
            message : "Successfully sent friend request."
        })
    } catch (e) {
        next(e);
    }
}

module.exports.getPendingRequests = async (req, res, next) => {
    try {
        const requests = await friendService.getRequestsSentByUser(req.user.id);
        return res.status(200).send(requests);
    } catch (e) {
        next(e)
    }
}

module.exports.getRequests = async (req, res, next) => {
    try {
        const requests = await friendService.getRequestsSentToUser(req.user.id);
        return res.status(200).send(requests);
    } catch (e) {
        next(e);
    }
}

module.exports.deleteRequest = async (req, res, next) => {
    try {
        if(!mongoose.Types.ObjectId.isValid(req.params.id)){
            return res.status(400).send("Invalid id");
        }
        const request = await friendService.findRequestbyId(req.params.id);
        if(!request){
            return res.status(400).send("No request with the given id found");
        }
        if(request.from != req.user.id){
            return res.status(401).send("You are not authorized");
        }
        await friendService.deleteRequest(req.params.id);
        return res.status(200).send("Successfully deleted request");
    } catch (e) {
        next(e);
    }
}

module.exports.acceptRequest = async (req, res, next) => {
    try {
        if(!mongoose.Types.ObjectId.isValid(req.params.id)){
            return res.status(400).send("Invalid id");
        }
        const request = await friendService.findRequestbyId(req.params.id);
        if(!request){
            return res.status(400).send("No request with the given id found");
        }
        if(request.to != req.user.id){
            return res.status(401).send("You are not authorized");
        }
        await friendService.acceptRequest(request);
        return res.status(200).send("Successfully accepted friend request");
    } catch (e) {
        next(e);
    }
}

module.exports.rejectRequest = async (req, res, next) => {
    try {
        if(!mongoose.Types.ObjectId.isValid(req.params.id)){
            return res.status(400).send("Invalid id");
        }
        const request = await friendService.findRequestbyId(req.params.id);
        if(!request){
            return res.status(400).send("No request with the given id found");
        }
        if(request.to != req.user.id){
            return res.status(401).send("You are not authorized");
        }
        await friendService.rejectRequest(request.id);
        return res.status(200).send("Successfully rejected friend request");
    } catch (e) {
        next(e);
    }
}

module.exports.getFriends = async (req, res, next) => {
    try {
        const page = req.query.page || 1;
        const friends = await friendService.getFriends(req.user, page);
        return res.status(200).send(friends);
    } catch (e) {
        next(e);
    }
}

module.exports.removeFriend = async (req, res, next) => {
    try {
        if(!mongoose.Types.ObjectId.isValid(req.params.id)){
            return res.status(400).send("Invalid id");
        }
        const user2 = await userService.getUserbyId(req.params.id);
        if(!user2){
            return res.status(400).send("No user with the id exists");
        }
        if(!friendService.checkFriend(req.user, req.params.id)){
            return res.status(200).send("You cannot remove a person who is not your friend.")
        }
        await friendService.removeFriend(req.user.id, user2.id);
        return res.status(200).send(`Successfully removed ${user2.username} as your friend.`);
    } catch (e) {
        next(e);
    }
}

module.exports.suggestFriends = async (req, res, next) => {
    try {
        const users = await friendService.suggestFriends(req.user);
        return res.status(200).send(users);
    } catch (e) {
        next(e);
    }
}