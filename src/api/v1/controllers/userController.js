const mongoose = require('mongoose');
const config = require("../../../config/config");
const cleanData = require('../services/clean_data');
const sendMail = require("../services/mail");
const signupTemplate = require("../../templates/signup");
const forgotPasswordTemplate = require("../../templates/forgotPassword");
const userService = require("../services/userService");
const unameService = require("../services/unameService");
const tokenService = require('../services/tokenService');

module.exports.signup = async (req, res, next) => {
    try{
        var clean_data = cleanData(req.body);
        
        var err = userService.validateDetails(clean_data);
        if(err){
            throw err;
        }

        const { username, password, email } = clean_data;
        const existing_users = await userService.findExistingUsers(username, email);

        if(existing_users){
            err = new Error('A user with the given username or email already exists!');
            err.status = 409;
            throw err;
        }
        
        const token = await tokenService.createActivationToken(username, password, email);
        const body = signupTemplate(req.hostname, username, token);

        await sendMail({
            to: email,
            subject: 'Activate your account',
            email_body: body
        });

        return res.status(200).send('Email sent successfully. Activate your account using the link sent through email.');
    }
    catch(e){
        await unameService.deleteUsername(req.body.username);
        next(e);
    }
}

module.exports.activate_account = async (req, res, next) => {
    try{
        const [err, details] = tokenService.verifyToken(req.params.token, config.jwt.types.ACTIVATION);
        
        if(err){
            const e = new Error("Invalid token");
            e.status = 400;
            throw e;
        }
        
        const ex = await userService.findUsersbyUsernameorEmail(details.username, details.email);
        
        if(ex.length > 0){
            return res.status(400).json({
                'message' : "An account with this username or email is already activated!"
            });
        }
        
        await userService.createUser(
            details.username,
            details.email,
            details.hash,
            isVerified=true
        );
        
        return res.status(201).json({
                'success' : true,
                "message" : "Successfully activated account"
            });

    }
    catch(e){
        next(e);
    }
}

module.exports.login = async (req, res, next) => {
    try{
        const clean = cleanData(req.body);
        const { username, password } = clean;

        const user = await userService.findUserandVerifyPassword(username, password);
        
        if(user){
            const token = tokenService.createAuthToken(user);

            return res.status(200).json({
                "success" : true,
                "message" : "Use this as a Bearer token",
                "token" : token
            })          
        }

        var err = new Error("Invalid credentials");
        err.status = 400;
        throw err;
    }
    catch(e){
        next(e);
    }
}

module.exports.getUsers = async (req, res, next) => {
    try{
        const users = await userService.getUsers(req.query);
        return res.status(200).json(users);
    }
    catch(e){
        next(e);
    }
}

module.exports.getUserbyId = async (req, res, next) => {
    try{
        if(!mongoose.Types.ObjectId.isValid(req.params.id)){
            return res.status(400).send("Invalid id");
        }
        const id = req.params.id;
        const details = await userService.getDetails(id);
        return res.status(200).json(details);
    }
    catch(e){
        next(e);
    }
}

module.exports.updateUserbyId = async (req, res, next) => {
    try{
        if(!mongoose.Types.ObjectId.isValid(req.params.id)){
            return res.status(400).send("Invalid id");
        }
        const id = req.params.id;
        if(req.user.id != id){
            var err = new Error("You are not authorized to change profile");
            err.status = 401;
            throw err;
        }
        const clean = cleanData(req.body);
        await userService.updateUserProfile(id, clean);
        res.status(200).send("Successfully updated your profile")
    }
    catch(e){
        next(e);
    }
}

module.exports.forgotPassword = async (req, res, next) => {
    try{
        const email = req.body.email;

        const user = await userService.getUserbyEmail(email);
        if(!user){
            var err = new Error("No user with the email exists");
            err.status = 400
            throw err;
        }
        const token = tokenService.createForgotPasswordToken(user);
        const body = forgotPasswordTemplate(req.hostname, user.username, token);

        await sendMail({
            to: email,
            subject: 'Reset your password',
            email_body: body
        });

        return res.status(200).send('Email sent successfully. Reset your password using the link sent through email.');


    }
    catch(e){
        next(e);
    }
}

module.exports.changePassword = async (req, res, next) => {
    try{
        const [err, details] = tokenService.verifyToken(req.params.token, config.jwt.types.FORGOT_PASSWORD);
        
        if(! (req.body.new_password && req.body.confirm_password)){
            const e = new Error("New password and confirm password are required");
            e.status = 400;
            throw e;
        }

        if(err){
            const e = new Error("Invalid token");
            e.status = 400;
            throw e;
        }

        if(req.body.new_password != req.body.confirm_password){
            const e = new Error("New password and confirm password are not matching");
            e.status = 400;
            throw e;
        }

        await userService.updateUserPassword(details.user_id, req.body.new_password);
        return res.status(200).send("Successfully updated passowrd");

    }
    catch(e){
        next(e);
    }
}