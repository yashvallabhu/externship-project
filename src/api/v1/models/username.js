const mongoose = require('mongoose');
const config = require("../../../config/config");

const usernameSchema = new mongoose.Schema({
    username: {
        type: String,
        required: true,
        unique: true
    },
    createdAt: { 
        type: Date, 
        expires: config.db.ACTIVATION_EXPIRY, 
        default: Date.now 
    }
});

module.exports = mongoose.model("Username", usernameSchema)