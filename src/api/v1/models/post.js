const mongoose = require('mongoose');

const postSchema = new mongoose.Schema({
    author : {
		type : mongoose.Schema.Types.ObjectId,
		ref: "User",
        required: true
    },
    image : {
        type: String,
        required: true
    },
    description : String,
    likes : [{
        type : mongoose.Schema.Types.ObjectId,
		ref: "User",
    }],
    comments: [{
        type : mongoose.Schema.Types.ObjectId,
		ref: "Comment",
    }]
}, { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } });

const Post = mongoose.model("Post", postSchema);

module.exports = Post