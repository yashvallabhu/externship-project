const mongoose = require('mongoose');
const Joi = require('joi');

const userSchema = new mongoose.Schema({
    username: {
        type: String,
        required: true,
        unique: true
    },
    hash: {
        type: String,
        required: true
    },
    email: {
		type: String,
		required: true,
        unique: true,
		match : /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/
	},
    first_name : String,
    last_name : String,
    isPublic: {
        type: Boolean,
        default: false
    },
    isVerified: {
        type: Boolean,
        default: false
    },
    friends : [{
        type : mongoose.Schema.Types.ObjectId,
		ref: "User"
    }],
    profile_pic : String
}, { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } })

const userDetailsValidate = (obj) => {
    const schema = Joi.object({
        username : Joi.string().min(4).required(),
        hash : Joi.string().required(),
        email : Joi.string().email().required(),
        first_name : Joi.string(),
        last_name : Joi.string(),
        isPublic : Joi.boolean().default(false),
        isVerified : Joi.boolean().default(false)
    });

    return schema.validate(obj);
} 

const User = mongoose.model("User", userSchema)

module.exports = {
    User,
    userDetailsValidate
}