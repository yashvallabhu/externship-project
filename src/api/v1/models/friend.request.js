const mongoose = require('mongoose');

const requestSchema = new mongoose.Schema({
    to : {
		type : mongoose.Schema.Types.ObjectId,
		ref: "User",
        required: true
    },
    from : {
		type : mongoose.Schema.Types.ObjectId,
		ref: "User",
        required: true
    }
}, { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } });

const FriendRequest = mongoose.model("FriendRequest", requestSchema);

module.exports = FriendRequest