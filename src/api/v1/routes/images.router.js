const express = require("express");
const router = express.Router();

const controller = require("../controllers/images.controller.js")
const checkAuth = require("../middlewares/auth.js");
const multer = require('multer')();

router.use(checkAuth);

router.post('/users/profile-pic', multer.single('image'), controller.uploadProfilePic);
router.post('/posts', multer.single('image'), controller.uploadPostImage);

module.exports = router;