const express = require("express");
const router = express.Router();

const controller = require("../controllers/friends.controller")
const checkAuth = require("../middlewares/auth.js");

router.use(checkAuth);

router.get("/suggestions", controller.suggestFriends);

router.post('/send-request', controller.sendRequest);

router.get('/requests/pending', controller.getPendingRequests);
router.get('/requests', controller.getRequests);
router.delete('/requests/:id', controller.deleteRequest);

router.post('/accept/:id', controller.acceptRequest);
router.post('/reject/:id', controller.rejectRequest);

router.get('/', controller.getFriends);
router.post('/remove/:id', controller.removeFriend);

module.exports = router