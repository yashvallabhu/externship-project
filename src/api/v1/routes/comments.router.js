const express = require("express");
const router = express.Router();

const controller = require("../controllers/comments.controller");
const checkAuth = require("../middlewares/auth.js");

router.use(checkAuth);

router.get('/:id', controller.getComments);
router.post('/:id', controller.createComment);
router.put('/:id', controller.updateComment);
router.delete('/:id', controller.deleteComment);

module.exports = router