const express = require("express");
const router = express.Router();

const controller = require("../controllers/userController")
const checkAuth = require("../middlewares/auth.js");

router.post("/signup", controller.signup);
router.post("/login", controller.login);
router.get("/activate_account/:token", controller.activate_account);

router.get("/forgot_password", controller.forgotPassword);
router.post("/forgot_password/:token", controller.changePassword);

router.get("/:id", controller.getUserbyId);
router.put("/:id", checkAuth, controller.updateUserbyId);

router.get("/", controller.getUsers);

module.exports = router