const express = require("express");
const router = express.Router();

const controller = require("../controllers/posts.controller")
const checkAuth = require("../middlewares/auth.js");

router.use(checkAuth);

router.get('/feed', controller.getFeed);
router.post('/like/:id', controller.likePost);

router.put('/:id', controller.updatePost);
router.delete('/:id', controller.deletePost);
router.get('/:id', controller.getPosts);

router.post('/', controller.createPost);

module.exports = router;